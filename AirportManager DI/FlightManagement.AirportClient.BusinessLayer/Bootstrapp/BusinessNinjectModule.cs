﻿using FlightManagement.AirportClient.BusinessLayer.Services;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Text;

namespace FlightManagement.AirportClient.BusinessLayer.Bootstrapp
{
    public class BusinessNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<IAirportService>().To<AirportService>();
            Kernel.Bind<IAirlaneService>().To<AirlaneService>(); 
            Kernel.Bind<IAirplaneService>().To<AirplaneService>();
            Kernel.Bind<IAirConnectionService>().To<AirConnectionService>();
            Kernel.Bind<IClientService>().To<ClientService>();
            Kernel.Bind<ITicketService>().To<TicketService>();
            Kernel.Bind<IAgeCalculatorService>().To<AgeCalculatorService>();
            Kernel.Bind<IDistanceCalculatorService>().To<DistanceCalculatorService>();
            Kernel.Bind<IDatabaseInitializer>().To<DatabaseInitializer>();
        }
    }
}
