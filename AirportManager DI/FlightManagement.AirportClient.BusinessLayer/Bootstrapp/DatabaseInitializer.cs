﻿using FlightManagement.AirportClient.DataLayer;
using Microsoft.EntityFrameworkCore;
using System;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Text;

namespace FlightManagement.AirportClient.BusinessLayer.Bootstrapp
{
    public interface IDatabaseInitializer
    {
        void Initialize();
    }

    public class DatabaseInitializer : IDatabaseInitializer
    {
        private readonly Func<IFlightManagementAirportClientDbContext> _dbContextFactory;

        public DatabaseInitializer(Func<IFlightManagementAirportClientDbContext> dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public void Initialize()
        {
            using (var ctx = _dbContextFactory())
            {
                ctx.Database.EnsureCreated();
            }
        }
    }
}
