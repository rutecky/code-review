﻿using FlightManagement.AirportClient.DataLayer;
using FlightManagement.AirportClient.DataLayer.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace FlightManagement.AirportClient.BusinessLayer.Services
{
    public interface ITicketService
    {
        int AddTicket(Client client, AirConnection connection);
        Ticket GetById(int id);
        void CancelTicket(Ticket ticket, Client client);
        List<Ticket> ShowHistory(int clientId);
        List<Ticket> ShowHistoryCancelledTickets(int clientId);
        List<Ticket> ShowValueAllTickets(int clientId);
        List<Ticket> ShowValueAllTicketsNotCancelled(int clientId);
        List<Ticket> ShowValueAllTicketsCancelled(int clientId);
        bool GetToJson(int clientId);
    }

    public class TicketService : ITicketService
    {
        private readonly Func<IFlightManagementAirportClientDbContext> _dbContextFactory;
        private readonly IDistanceCalculatorService _distanceCalculatorService;
        private readonly IAgeCalculatorService _ageCalculatorService;

        public TicketService(Func<IFlightManagementAirportClientDbContext> dbContextFactory,
            IDistanceCalculatorService distanceCalculatorService,
            IAgeCalculatorService ageCalculatorService)
        {
            _dbContextFactory = dbContextFactory;
            _distanceCalculatorService = distanceCalculatorService;
            _ageCalculatorService = ageCalculatorService;
        }
        public int AddTicket(Client client, AirConnection connection)
        {
            using (var context = _dbContextFactory())
            {
                var discount = _ageCalculatorService.CalculateAgeAndGetDiscount(client.DateOfBorn);
                var getDistance = _distanceCalculatorService.GetDistanceToAirport(connection.AirportLatitude,connection.AirportLongitude);
                double loyalityBonus = 1;

                if(getDistance > 10000)
                {
                    loyalityBonus = 0.75;
                }                

                var newTicket = new Ticket
                {                    
                    OwnerId = client.Id,
                    ConnectionId = connection.Id,
                    Price = (connection.Price - (connection.Price * discount)) * loyalityBonus,
                    Key = connection.Key,
                    AirportCity = connection.AirportCity,
                    NumberOfDays = connection.NumberOfDays,
                    dateTime = connection.dateTime,
                    isCanceled = false,
                    Distance = getDistance
                };
                var entity = context.Tickets.Add(newTicket);

                client.DistanceTravelled += getDistance;

                context.Clients.Update(client);
                context.SaveChanges();

                return entity.Entity.Id;
            }
        }

        public Ticket GetById(int id)
        {
            using (var context = _dbContextFactory())
            {
                return context.Tickets
                    .FirstOrDefault(ticket => ticket.Id == id);
            }
        }

        public void CancelTicket(Ticket ticket, Client client)
        {
            using (var context = _dbContextFactory())
            {
                if (ticket.isCanceled == true)
                {
                    Console.WriteLine("This ticket has cancelled before.");
                }

                else
                {
                    ticket.isCanceled = true;
                    client.DistanceTravelled -= ticket.Distance;
                    context.Tickets.Update(ticket);
                    context.Clients.Update(client);
                    context.SaveChanges();
                    Console.WriteLine("Ticket cancelled successfully");
                }
            }
        }

        public List<Ticket> ShowHistory(int clientId)
        {
            using (var context = _dbContextFactory())
            {
                var ticketHistory = context.Tickets
                    .Where(ticket => ticket.isCanceled == false && ticket.OwnerId == clientId)
                    .Select(ticket => new Ticket
                    {
                        Id = ticket.Id,
                        Key = ticket.Key,
                        AirportCity = ticket.AirportCity,
                        dateTime = ticket.dateTime,
                        Price = ticket.Price,
                        NumberOfDays = ticket.NumberOfDays,
                        OwnerId = ticket.OwnerId,
                        Distance = ticket.Distance                        
                    })
                    .ToList()
                    .OrderByDescending(ticket => ticket.NumberOfDays)
                    .ToList();

                return ticketHistory;
            }
        }

        public List<Ticket> ShowHistoryCancelledTickets(int clientId)
        {
            using (var context = _dbContextFactory())
            {
                var ticketHistoryCancelledTickets = context.Tickets
                    .Where(ticket => ticket.isCanceled == true && ticket.OwnerId == clientId)
                    .Select(ticket => new Ticket
                    {
                        Id = ticket.Id,
                        Key = ticket.Key,
                        AirportCity = ticket.AirportCity,
                        dateTime = ticket.dateTime,
                        Price = ticket.Price,
                        NumberOfDays = ticket.NumberOfDays,
                        OwnerId = ticket.OwnerId,
                        Distance = ticket.Distance
                    })
                    .ToList()
                    .OrderByDescending(ticket => ticket.NumberOfDays)
                    .ToList();

                return ticketHistoryCancelledTickets;
            }
        }

        public List<Ticket> ShowValueAllTickets(int clientId)
        {
            using (var context = _dbContextFactory())
            {
                var valueTicketsNotCancelled = context.Tickets
                    .Where(ticket => ticket.OwnerId == clientId)
                    .Select(ticket => new Ticket
                    {
                        Price = ticket.Price
                    })
                    .ToList();

                return valueTicketsNotCancelled;
            }
        }

        public List<Ticket> ShowValueAllTicketsNotCancelled(int clientId)
        {
            using (var context = _dbContextFactory())
            {
                var valueTicketsNotCancelled = context.Tickets
                    .Where(ticket => ticket.isCanceled == false && ticket.OwnerId == clientId)
                    .Select(ticket => new Ticket
                    {
                        Price = ticket.Price
                    })
                    .ToList();

                return valueTicketsNotCancelled;
            }
        }

        public List<Ticket> ShowValueAllTicketsCancelled(int clientId)
        {
            using (var context = _dbContextFactory())
            {
                var valueTicketsCancelled = context.Tickets
                    .Where(ticket => ticket.isCanceled == true && ticket.OwnerId == clientId)
                    .Select(ticket => new Ticket
                    {
                        Price = ticket.Price
                    })
                    .ToList();

                return valueTicketsCancelled;
            }
        }
        public bool GetToJson(int clientId)
        {
            Console.WriteLine("Enter the destination of file: ");
            var inputFromUser = Console.ReadLine();
            File.WriteAllText($"{inputFromUser}.json", JsonConvert.SerializeObject(ShowHistory(clientId)));

            using (StreamWriter file = File.CreateText($"{inputFromUser}.json"))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, ShowHistory(clientId));
            }

            return true;
        }        
    }
}
