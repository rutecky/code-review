﻿using FlightManagement.AirportClient.DataLayer;
using FlightManagement.AirportClient.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightManagement.AirportClient.BusinessLayer.Services
{
    public interface IClientService
    {
        int AddClient(Client client);
        IEnumerable<Client> GetAllClients();
        void UpdateClient(Client client, Client updateClient);
        Client GetById(int id);
    }

    public class ClientService : IClientService
    {
        private readonly Func<IFlightManagementAirportClientDbContext> _dbContextFactory;        

        public ClientService(Func<IFlightManagementAirportClientDbContext> dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public int AddClient(Client client)
        {
            using (var context = _dbContextFactory())
            {
                var entity = context.Clients.Add(client);
                context.SaveChanges();
                return entity.Entity.Id;
            }
        }

        public Client GetById(int id)
        {
            using (var context = _dbContextFactory())
            {
                return context.Clients
                    .FirstOrDefault(client => client.Id == id);
            }
        }

        public void UpdateClient(Client client, Client updateClient)
        {
            using (var context = _dbContextFactory())
            {
                Console.WriteLine($"OLD VALUES: ID: {client.Id} , Name: {client.Name} , Surname: {client.Surname} , " +
                    $"Date of born: {client.DateOfBorn.Day}/{client.DateOfBorn.Month}/{client.DateOfBorn.Year} Travelled: {client.DistanceTravelled}");
                if (updateClient.Name == "")
                {
                    updateClient.Name = client.Name;
                }
                if (updateClient.Surname == "")
                {
                    updateClient.Surname = client.Surname;
                }
                if (updateClient.DateOfBorn == client.DateOfBorn)
                {
                    Console.WriteLine("Date of born is the same like before. Nice!");
                }
                context.Clients.Update(updateClient);
                context.SaveChanges();
                Console.WriteLine($"UPDATE VALUES: ID: {updateClient.Id} , Name: {updateClient.Name} , Surname: {updateClient.Surname} , " +
                    $"Date of born: {updateClient.DateOfBorn.Day}/{updateClient.DateOfBorn.Month}/{updateClient.DateOfBorn.Year} Travelled: {updateClient.DistanceTravelled}");
            }
        }

        public IEnumerable<Client> GetAllClients()
        {
            using (var context = _dbContextFactory())
            {
                return context.Clients.ToList();
            }
        }
    }
}
