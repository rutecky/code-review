﻿using FlightManagement.AirportClient.DataLayer;
using FlightManagement.AirportClient.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightManagement.AirportClient.BusinessLayer.Services
{
    public interface IAirlaneService
    {
        int AddAirlane(Airlane airlane);
        IEnumerable<Airlane> GetAllAirlanes();
        Airlane GetAirlaneId(int IdAirlane);
        Airlane GetAirlaneName(string nameAirlane);
        Airlane GetAirlaneKey(string keyAirlane);
    }

    public class AirlaneService : IAirlaneService
    {
        private readonly Func<IFlightManagementAirportClientDbContext> _dbContextFactory;

        public AirlaneService(Func<IFlightManagementAirportClientDbContext> dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public int AddAirlane(Airlane airlane)
        {
            using (var context = _dbContextFactory())
            {
                var newAirlane = new Airlane
                {
                    Id = airlane.Id,
                    Name = airlane.Name,
                    Key = airlane.Key
                };
                if (newAirlane.Key.Length == 2)
                {
                    var entity = context.Airlanes.Add(newAirlane);
                    context.SaveChanges();
                    Console.WriteLine($"Airlane created successfully. ID = {newAirlane.Id}");
                }
                else
                {
                    Console.WriteLine("Error occurred. Key is wrong");
                }
                return newAirlane.Id;
            }
        }

        public IEnumerable<Airlane> GetAllAirlanes()
        {
            using (var context = _dbContextFactory())
            {
                return context.Airlanes.ToList();
            }
        }

        public Airlane GetAirlaneId(int IdAirlane)
        {
            using (var context = _dbContextFactory())
            {
                return context.Airlanes.First(x => x.Id == IdAirlane);
            }
        }

        public Airlane GetAirlaneName(string nameAirlane)
        {
            using (var context = _dbContextFactory())
            {
                return context.Airlanes.First(x => x.Name == nameAirlane);
            }
        }

        public Airlane GetAirlaneKey(string keyAirlane)
        {
            using (var context = _dbContextFactory())
            {
                return context.Airlanes.First(x => x.Key == keyAirlane);
            }
        }
    }
}
