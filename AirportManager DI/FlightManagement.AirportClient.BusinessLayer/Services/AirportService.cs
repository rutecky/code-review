﻿using FlightManagement.AirportClient.DataLayer;
using FlightManagement.AirportClient.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightManagement.AirportClient.BusinessLayer.Services
{
    public interface IAirportService
    {
        int AddAirport(Airports airport);
        IEnumerable<Airports> GetAllAirports();
        Airports GetAirportId(int IdAirport);
        Airports GetAirportCity(string cityAirport);
    }

    public class AirportService : IAirportService
    {
        private readonly Func<IFlightManagementAirportClientDbContext> _dbContextFactory;

        public AirportService(Func<IFlightManagementAirportClientDbContext> dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public int AddAirport(Airports airport)
        {
            using (var context = _dbContextFactory())
            {
                var entity = context.Airports.Add(airport);
                context.SaveChanges();
                return entity.Entity.Id;
            }
        }

        public IEnumerable<Airports> GetAllAirports()
        {
            using (var context =  _dbContextFactory())
            {
                return context.Airports.ToList();
            }
        }

        public Airports GetAirportId(int IdAirport)
        {
            using (var context = _dbContextFactory())
            {
                return context.Airports.First(x => x.Id == IdAirport);
            }
        }

        public Airports GetAirportCity(string cityAirport)
        {
            using (var context = _dbContextFactory())
            {
                return context.Airports.First(x => x.AirportCityName == cityAirport);
            }
        }
    }
}
