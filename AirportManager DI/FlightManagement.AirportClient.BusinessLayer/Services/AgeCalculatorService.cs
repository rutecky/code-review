﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlightManagement.AirportClient.BusinessLayer.Services
{
    public interface IAgeCalculatorService
    {
        double CalculateAgeAndGetDiscount(DateTime birthDate);
    }

    public class AgeCalculatorService : IAgeCalculatorService
    {
        public double CalculateAgeAndGetDiscount(DateTime birthDate)
        {
            DateTime now = DateTime.Now;
            double discount = 1;
            int age = now.Year - birthDate.Year;

            if (birthDate > now.AddYears(-age)) 
                age--;

            if(age > 15)
            {
                discount = 0;
            }

            if(age < 15 && age >= 3)
            {
                discount = 0.5;
            }

            if(age<0)
            { 
                Console.WriteLine("It's impossible! Becouse you're from future, you have extra discount 200% !!!");
                discount = 2;
            }

            return discount;
        }
    }
}
