﻿using FlightManagement.AirportClient.DataLayer;
using FlightManagement.AirportClient.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightManagement.AirportClient.BusinessLayer.Services
{
    public interface IAirConnectionService
    {
        int AddConnection(Airplane airplane, Airlane airlane, Airports airport, int connectionKey, int connectionDate, double connectionPrice, DateTime time);
        IEnumerable<AirConnection> GetAllAirConnections();
        AirConnection GetById(int id);
    }

    public class AirConnectionService : IAirConnectionService
    {
        private readonly Func<IFlightManagementAirportClientDbContext> _dbContextFactory;

        public AirConnectionService(Func<IFlightManagementAirportClientDbContext> dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }
        public int AddConnection(Airplane airplane, Airlane airlane,Airports airport,int connectionKey,int connectionDate,double connectionPrice, DateTime time)
        {
            using (var context = _dbContextFactory())
            {

                var newConnection = new AirConnection
                {
                    AirplaneModel = airplane.Model,
                    AirportCity = airport.AirportCityName,
                    Key = airlane.Key + "-" + connectionKey.ToString(),
                    NumberOfDays = connectionDate,
                    Price = connectionPrice,
                    dateTime = time,
                    AirportLatitude = airport.Latitude,
                    AirportLongitude = airport.Longitude,
                };

                var entity = context.AirConnections.Add(newConnection);
                context.SaveChanges();
                return entity.Entity.Id;
            }
        }
        public IEnumerable<AirConnection> GetAllAirConnections()
        {
            using (var context = _dbContextFactory())
            {
                return context.AirConnections.ToList();
            }
        }
        public AirConnection GetById(int id)
        {
            using (var context = _dbContextFactory())
            {
                return context.AirConnections
                    .FirstOrDefault(connection => connection.Id == id);
            }
        }
    }
}
