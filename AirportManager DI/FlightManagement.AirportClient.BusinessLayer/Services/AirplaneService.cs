﻿using FlightManagement.AirportClient.DataLayer;
using FlightManagement.AirportClient.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightManagement.AirportClient.BusinessLayer.Services
{
    public interface IAirplaneService
    {
        int AddAirplane(Airplane airplane);
        IEnumerable<Airplane> GetAllAirplanes();
        Airplane GetAirplaneId(int IdAirplane);
        Airplane GetAirplaneModel(string modelAirplane);
    }

    public class AirplaneService : IAirplaneService
    {
        private readonly Func<IFlightManagementAirportClientDbContext> _dbContextFactory;

        public AirplaneService(Func<IFlightManagementAirportClientDbContext> dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public int AddAirplane(Airplane airplane)
        {
            using (var context = _dbContextFactory())
            {
                var entity = context.Airplanes.Add(airplane);
                context.SaveChanges();
                return entity.Entity.Id;
            }
        }

        public IEnumerable<Airplane> GetAllAirplanes()
        {
            using (var context = _dbContextFactory())
            {
                return context.Airplanes.ToList();
            }
        }

        public Airplane GetAirplaneId(int IdAirplane)
        {
            using (var context = _dbContextFactory())
            {
                return context.Airplanes.First(x => x.Id == IdAirplane);
            }
        }

        public Airplane GetAirplaneModel(string modelAirplane)
        {
            using (var context = _dbContextFactory())
            {
                return context.Airplanes.First(x => x.Model == modelAirplane);
            }
        }
    }
}
