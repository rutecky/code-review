﻿using FlightManagement.AirportClient.DataLayer;
using FlightManagement.AirportClient.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace FlightManagement.AirportClient.BusinessLayer.Services
{
    public interface IDistanceCalculatorService
    {
        double deg2rad(double deg);
        double GetDistanceToAirport(double longitude, double latitude);
        double rad2deg(double rad);
    }

    public class DistanceCalculatorService : IDistanceCalculatorService
    {      

        public double GetDistanceToAirport(double latitude, double longitude)
        {
            double myAirportLatitude = 22.22;
            double myAirportLongitude = 55.55;

            if (myAirportLatitude == latitude && myAirportLongitude == longitude)
            {
                return 0;
            }

            else
            {
                double theta = myAirportLongitude - longitude;
                double dist = Math.Sin(deg2rad(myAirportLatitude)) * Math.Sin(deg2rad(latitude)) + Math.Cos(deg2rad(myAirportLatitude)) * Math.Cos(deg2rad(latitude)) * Math.Cos(deg2rad(theta));
                dist = Math.Acos(dist);
                dist = rad2deg(dist);
                dist = dist * 60 * 1.1515;
                return (dist);
            }
        } // liczenie wzięte stąd https://www.geodatasource.com/developers/c-sharp

        public double deg2rad(double deg)
        {
            return (deg * Math.PI / 180.0);
        }

        public double rad2deg(double rad)
        {
            return (rad / Math.PI * 180.0);
        }
    }
}
