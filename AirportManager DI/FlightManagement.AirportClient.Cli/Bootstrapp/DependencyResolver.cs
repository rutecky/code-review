﻿using FlightManagement.AirportClient.DataLayer.Bootstrapp;
using FlightManagement.AirportClient.BusinessLayer.Bootstrapp;
using Ninject;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Text;

namespace FlightManagement.AirportClient.Cli.Bootstrapp
{
    public static class DependencyResolver
    {
        public static IKernel GetKernel()
        {
            var kernel = new StandardKernel();

            kernel.Load(new INinjectModule[]
            {
              new CliNinjectModule(),
              new BusinessNinjectModule(),
              new DataLayerNinjectModule()
            });

            return kernel;
        }
    }
}
