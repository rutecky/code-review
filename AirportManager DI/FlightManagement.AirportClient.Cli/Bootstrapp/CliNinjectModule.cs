﻿using FlightManagement.AirportClient.Cli.Helpers;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Text;

namespace FlightManagement.AirportClient.Cli.Bootstrapp
{
    public class CliNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<IProgram>().To<Program>();
            Kernel.Bind<IIoHelper>().To<IoHelper>();
            Kernel.Bind<IMenu>().To<Menu>();
        }
    }
}
