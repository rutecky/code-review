﻿using FlightManagement.AirportClient.BusinessLayer.Bootstrapp;
using FlightManagement.AirportClient.BusinessLayer.Services;
using FlightManagement.AirportClient.Cli.Bootstrapp;
using FlightManagement.AirportClient.Cli.Helpers;
using FlightManagement.AirportClient.DataLayer.Models;
using System.Text.RegularExpressions;
using Ninject;
using System;

namespace FlightManagement.AirportClient.Cli
{
    public interface IProgram
    {
        void Run();
    }

    internal class Program : IProgram
    {
        private readonly IAirlaneService _airlaneService;
        private readonly IAirConnectionService _airConnectionService;
        private readonly IAirplaneService _airplaneService;
        private readonly IClientService _clientService;
        private readonly IAirportService _airportService;
        private readonly ITicketService _ticketService;
        private readonly IDatabaseInitializer _databaseInitializer;        
        private readonly IIoHelper _ioHelper;
        private readonly IMenu _menu;

        static void Main(string[] args)
        {
            using (var kernel = DependencyResolver.GetKernel())
            {
                var program = kernel.Get<IProgram>();
                program.Run();
            }
        }


        public Program(
            IAirlaneService airlaneService,
            IAirConnectionService airConnectionService,
            IAirplaneService airplaneService,
            IClientService clientService,
            IAirportService airportService,
            ITicketService ticketService,
            IDatabaseInitializer databaseInitializer,
            IIoHelper ioHelper,
            IMenu menu
            )
        {
            _airlaneService = airlaneService;
            _airConnectionService = airConnectionService;
            _airplaneService = airplaneService;
            _clientService = clientService;
            _airportService = airportService;
            _ticketService = ticketService;
            _databaseInitializer = databaseInitializer;
            _ioHelper = ioHelper;
            _menu = menu;
        }

        public void Run()
        {
            InitializeDatabase();
            InitializeMenuItems();

            while (true)
            {
                _menu.ShowMenu();

                var input = _ioHelper.GetIntFromUser("Enter command number");

                Console.Clear();

                try
                {
                    _menu.ExecuteAction(input);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Command execution failed: {e.Message}");
                }
            }
        }

        private void InitializeDatabase()
        {
            _databaseInitializer.Initialize();
        }

        private void InitializeMenuItems()
        {
            _menu.AddMenuItem(CreateAirport,         "Create Airport",          1);
            _menu.AddMenuItem(CreateAirplane,        "Create Airplane",         2);
            _menu.AddMenuItem(CreateAirlane,         "Create Airlane",          3);
            _menu.AddMenuItem(CreateAirConnection,   "Create AirConnection",    4);
            _menu.AddMenuItem(CreateClient,          "Create Client",           5);

            _menu.AddMenuItem(ShowAllAirports,       "Show all Airports",       6);
            _menu.AddMenuItem(ShowAllAirplanes,      "Show all Airplanes",      7);
            _menu.AddMenuItem(ShowAllAirlanes,       "Show all Airlanes",       8);
            _menu.AddMenuItem(ShowAllAirConnections, "Show all AirConnections", 9);
            _menu.AddMenuItem(ShowAllClients,        "Show all Clients",       10);

            _menu.AddMenuItem(UpdateClient,          "Update Client",          11);
            _menu.AddMenuItem(SellTicket,            "Sell Ticket",            12);
            _menu.AddMenuItem(CancelTicket,          "Cancel ticket",          13);
            _menu.AddMenuItem(ShowTicketHistory,     "Show ticket history",    14);
            _menu.AddMenuItem(Exit,                  "Close application",      99);
        }

        private void CreateAirport()
        {
            Console.WriteLine("Creating new Airport. Please enter following data:");
            
            var newAirport = new Airports
            {
                AirportName = _ioHelper.GetStringFromUser("Enter a name of airport"),
                AirportCityName = _ioHelper.GetStringFromUser("Enter a city of airport"),
                AirportCountryName = _ioHelper.GetStringFromUser("Enter a country of airport"),
                Latitude = _ioHelper.GetLatitudeFromUser("Enter a latitude of airport. Range <-90;90>, use \",\" between geographical deegres"),
                Longitude = _ioHelper.GetLongitudeFromUser("Enter a longitude of airport. Range <-180;180> , use \",\" between geographical deegres")
            };

            var airportId = _airportService.AddAirport(newAirport);

            Console.WriteLine($"Airport created. ID = {airportId}");

        }
        private void CreateAirplane()
        {
            Console.WriteLine("Creating new Airplane. Please enter following data:");

            var newAirplane = new Airplane
            {
                Producer = _ioHelper.GetStringFromUser("Enter a Producer."),
                Model = _ioHelper.GetStringFromUser("Enter a Model"),
                NumberOfSeats = _ioHelper.GetIntFromUser("Enter a number of seats")
            };

            var airplaneId = _airplaneService.AddAirplane(newAirplane);

            Console.WriteLine($"Airplane created. ID = {airplaneId}");
        }

        private void CreateAirlane()
        {
            Console.WriteLine("Creating new Airlane. Please enter following data:");
            
            var newAirlane = new Airlane
            {
                Name = _ioHelper.GetStringFromUser("Enter a name"),
                Key = _ioHelper.GetStringFromUser("Enter a key. Two symbols.")
            };
            try
            {
                _airlaneService.AddAirlane(newAirlane);
            }

            catch
            {
                Console.WriteLine("Error occurred");
            }
        }

        private void CreateAirConnection()
        {
            Console.WriteLine("Creating new AirConnection. Please enter following data:");

            var IdAirplane = _ioHelper.GetIntFromUser("Enter ID of Airplane");
            var airplane = _airplaneService.GetAirplaneId(IdAirplane);

            var IdAirlane = _ioHelper.GetIntFromUser("Enter ID of Airlane");
            var airlane = _airlaneService.GetAirlaneId(IdAirlane);

            var IdAirport = _ioHelper.GetIntFromUser("Enter ID of Airport");
            var airport = _airportService.GetAirportId(IdAirport);

            var connectionKey = _ioHelper.GetKeyFromUser("Enter a KEY of this connection. 5x number.");
            var connectionDate = _ioHelper.GetDayFromUser("Enter a day. 1 - Today, 2 - Tommorow etc.");
            var connectionPrice = _ioHelper.GetDoubleFromUser("Enter a price of ticket.");
            var time = _ioHelper.GetTimeFromUser("Enter a hour. HH:mm");

            try
            {
                var newConnection = _airConnectionService.AddConnection(airplane, airlane, airport, connectionKey,connectionDate,connectionPrice,time);
                Console.WriteLine($"Connection created successfully. ID = {newConnection}");
            }

            catch
            {
                Console.WriteLine("Error occured.");
            }
        }

        private void CreateClient()
        {
            Console.WriteLine("Creating new Client. Please enter following data:");

            var newClient = new Client
            {
                Name = _ioHelper.GetStringFromUser("Enter a Name:"),
                Surname = _ioHelper.GetStringFromUser("Enter a Surname:"),
                DateOfBorn = _ioHelper.GetDateFromUser("Enter a date of born"),
                DistanceTravelled = 0
            };

            var clientId = _clientService.AddClient(newClient);

            Console.WriteLine($"Client created. ID = {clientId}");
        }

        private void ShowAllAirports()
        {
            var airports = _airportService.GetAllAirports();

            foreach (var airport in airports)
            {
                _ioHelper.PrintAirport(airport);
            }
        }

        private void ShowAllAirplanes()
        {
            var airplanes = _airplaneService.GetAllAirplanes();

            foreach (var airplane in airplanes)
            {
                _ioHelper.PrintAirplane(airplane);
            }
        }

        private void ShowAllAirlanes()
        {
            var airlanes = _airlaneService.GetAllAirlanes();

            foreach (var airlane in airlanes)
            {
                _ioHelper.PrintAirlane(airlane);
            }
        }

        private void ShowAllAirConnections()
        {
            var connections = _airConnectionService.GetAllAirConnections();

            foreach (var connection in connections)
            {
                _ioHelper.PrintAirConnection(connection);
            }
        }

        private void ShowAllClients()
        {
            var clients = _clientService.GetAllClients();

            foreach (var client in clients)
            {
                _ioHelper.PrintClient(client);
            }
        }

        private void UpdateClient()
        {
            var clientId = _ioHelper.GetIntFromUser("Enter a ID of user to update");
            var client = _clientService.GetById(clientId);

            var updateClient = new Client
            {
                Id = client.Id,
                Name = _ioHelper.GetStringFromUser($"Current name: {client.Name} - Skip: press ENTER. Update: input new value."),
                Surname = _ioHelper.GetStringFromUser($"Current surname: {client.Surname} - Skip - press ENTER. Update - input new value."),
                DateOfBorn = _ioHelper.GetDateFromUser($"Current Date: {client.DateOfBorn.Day}/{client.DateOfBorn.Month}/{client.DateOfBorn.Year}. Update or input the same values. dd-MM-yyyy"),
                DistanceTravelled = client.DistanceTravelled
            };          

            _clientService.UpdateClient(client, updateClient);
        }

        private void SellTicket()
        {
            var clientId = _ioHelper.GetIntFromUser("Enter a ID of user to buy ticket");
            var client = _clientService.GetById(clientId);

            var connectionId = _ioHelper.GetIntFromUser("Enter a ID of connection to buy ticket");
            var connection = _airConnectionService.GetById(connectionId);

            try
            {                
                var sellTicketId = _ticketService.AddTicket(client, connection);
                Console.WriteLine($"Ticket sold successfully. ID = {sellTicketId}");
            }

            catch
            {
                Console.WriteLine("Error occured.");
            }
        }

        private void CancelTicket()
        {
            var clientId = _ioHelper.GetIntFromUser("Enter a ID of user to cancel ticket");
            var client = _clientService.GetById(clientId);

            var ticketId = _ioHelper.GetIntFromUser("Enter a ID of ticket to cancel");
            var ticket = _ticketService.GetById(ticketId);

            try
            {
                if (client.Id == ticket.OwnerId)
                {
                    _ticketService.CancelTicket(ticket, client);
                }
            }

            catch
            {
                Console.WriteLine("Error occured. Wrong ID!");
            }
        }

        private void ShowTicketHistory()
        {
            var clientId = _ioHelper.GetIntFromUser("Enter a ID of user to get ticket history.");
            var ticketHistory = _ticketService.ShowHistory(clientId);

            Console.WriteLine("Not cancelled tickets. \nTicketID ConnectionKey NumberOfDay Hour Airport Price OwnerID Distance");

            foreach (var history in ticketHistory)
            {
                _ioHelper.PrintTicketHistory(history);
            }

            var ticketHistoryCanceled = _ticketService.ShowHistoryCancelledTickets(clientId);
            Console.WriteLine("Cancelled tickets. \nTicketID ConnectionKey NumberOfDay Hour Airport Price OwnerID Distance");

            foreach (var history in ticketHistoryCanceled)
            {
                _ioHelper.PrintTicketHistory(history);

            }

            var allTicketsValue = _ticketService.ShowValueAllTickets(clientId);
            Console.WriteLine("Price of all tickets");
            double costAllTickets = 0;

            foreach(var history in allTicketsValue)
            {
                costAllTickets += history.Price;
            }

            Console.WriteLine($"{costAllTickets}");

            var notCancelledTicketValue = _ticketService.ShowValueAllTicketsNotCancelled(clientId);
            Console.WriteLine("Price of all tickets not cancelled: ");
            double costNotCancelledTickets = 0;

            foreach (var history in notCancelledTicketValue)
            {
                costNotCancelledTickets += history.Price;
            }

            Console.WriteLine($"{costNotCancelledTickets}");

            var CancelledTicketValue = _ticketService.ShowValueAllTicketsCancelled(clientId);
            Console.WriteLine("Price of all tickets cancelled: ");
            double costCancelledTickets = 0;

            foreach (var history in CancelledTicketValue)
            {
                costCancelledTickets += history.Price;
            }

            Console.WriteLine($"{costCancelledTickets}");

            var inputFromUser = _ioHelper.GetIntFromUser("Do you want to export this raport to Json?\n 1 - Yes \n 2 - No \n You choose");

            if (inputFromUser == 1)
            {
                _ticketService.GetToJson(clientId);
            }
        }

        public void Exit()
        {
            Environment.Exit(0);
        }
    }
}
