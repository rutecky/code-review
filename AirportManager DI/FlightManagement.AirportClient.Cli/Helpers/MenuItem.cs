﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlightManagement.AirportClient.Cli.Helpers
{
    internal class MenuItem
    {
        public string Description;
        public Action Action;
    }
}

