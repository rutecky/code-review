﻿using FlightManagement.AirportClient.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;


namespace FlightManagement.AirportClient.Cli.Helpers
{
    internal interface IIoHelper
    {
        DateTime GetTimeFromUser(string message);
        DateTime GetDateFromUser(string message);
        double GetDoubleFromUser(string message);
        double GetLatitudeFromUser(string message);
        double GetLongitudeFromUser(string message);
        int GetIntFromUser(string message);
        string GetStringFromUser(string message);
        int GetKeyFromUser(string message);
        int GetDayFromUser(string message);
        void PrintAirport(Airports airport);
        void PrintAirplane(Airplane airplane);
        void PrintAirlane(Airlane airlane);
        void PrintAirConnection(AirConnection airconnection);
        void PrintClient(Client client);
        void PrintTicketHistory(Ticket history);
    }

    internal class IoHelper : IIoHelper
    {
        public DateTime GetTimeFromUser(string message)
        {
            const string dateFormat = "HH:mm";
            var result = new DateTime();
            var success = false;
            while (!success)
            {
                Console.Write($"{message} ({dateFormat}): ");
                var input = Console.ReadLine();
                success = DateTime.TryParseExact(
                    input,
                    dateFormat,
                    CultureInfo.InvariantCulture,
                    DateTimeStyles.None,
                    out result);

                if (!success)
                {
                    Console.WriteLine("It was not a date in a correct format. Try again...");
                }
            }

            return result;
        }
        public DateTime GetDateFromUser(string message)
        {
            const string dateFormat = "dd-MM-yyyy";
            var result = new DateTime();
            var success = false;

            while (!success)
            {
                Console.Write($"{message} ({dateFormat}): ");
                var input = Console.ReadLine();
                success = DateTime.TryParseExact(
                    input,
                    dateFormat,
                    CultureInfo.InvariantCulture,
                    DateTimeStyles.None,
                    out result);
                if(result > DateTime.Now)
                {
                    success = false;
                    Console.WriteLine("You cant put a day from future :D");
                }
                if (!success)
                {
                    Console.WriteLine("It was not a date in a correct format. Try again...");
                }
            }

            return result;
        }

        public int GetIntFromUser(string message)
        {
            int result = 0;
            var success = false;

            while (!success)
            {
                Console.Write($"{message}: ");
                var input = Console.ReadLine();
                success = int.TryParse(input, out result);

                if (!success)
                {
                    Console.WriteLine("It was not a number. Try again...");
                }
            }

            return result;
        }

        public int GetDayFromUser(string message)
        {
            int result = 0;
            var success = false;

            while (!success)
            {
                Console.Write($"{message}: ");
                var input = Console.ReadLine();
                success = int.TryParse(input, out result) && result > 0 && result < 8;
                if (!success)
                {
                    Console.WriteLine("It was not a correct number. Try again...");                    
                }
            }

            return result;          
        }

        public int GetKeyFromUser(string message)
        {
            int result = 0;
            var success = false;

            while (!success)
            {
                Console.Write($"{message}: ");
                var input = Console.ReadLine();
                success = int.TryParse(input, out result) && input.Length == 5;

                if (!success)
                {
                    Console.WriteLine("It was not a correct number. Try again...");
                }
            }

            return result;
        }

       
        public double GetDoubleFromUser(string message)
        {
            double result = 0;
            var success = false;

            while (!success)
            {
                Console.Write($"{message}: ");
                var input = Console.ReadLine();
                success = double.TryParse(input, out result);

                if (!success)
                {
                    Console.WriteLine("It was not a number");
                }
            }

            return result;
        }
        public double GetLatitudeFromUser(string message)
        {
            double result = 0;
            var success = false;

            while (!success)
            {
                Console.Write($"{message}: ");
                var input = Console.ReadLine();
                success = double.TryParse(input, out result) && result >= -90 && result <= 90;

                if (!success)
                {
                    Console.WriteLine("It was not a correct geographical coordinate");
                }
            }

            return result;
        }
        public double GetLongitudeFromUser(string message)
        {
            double result = 0;
            var success = false;

            while (!success)
            {
                Console.Write($"{message}: ");
                var input = Console.ReadLine();
                success = double.TryParse(input, out result) && result >= -180 && result <= 180;

                if (!success)
                {
                    Console.WriteLine("It was not a correct geographical coordinate");
                }
            }

            return result;
        }

        public string GetStringFromUser(string message)
        {
            Console.Write($"{message}: ");
            return Console.ReadLine();
        }

        public void PrintAirport(Airports airport)
        {
            Console.WriteLine($"Airport ID: {airport.Id}  Name: {airport.AirportName} City : {airport.AirportCityName} Country: {airport.AirportCountryName} " +
                $"Coordinates - Longitude : {airport.Longitude} Latitude : {airport.Latitude}");
        }
        public void PrintAirplane(Airplane airplane)
        {
            Console.WriteLine($"Airplane ID: {airplane.Id} Producer: {airplane.Producer} Model: {airplane.Model} Seats: {airplane.NumberOfSeats}");
        }
        public void PrintAirlane(Airlane airlane)
        {
            Console.WriteLine($"Airlane ID: {airlane.Id} Name: {airlane.Name} Key: {airlane.Key}");
        }
        public void PrintAirConnection(AirConnection airConnection)
        {
            Console.WriteLine($"AirConnection ID: {airConnection.Id} Airport City: {airConnection.AirportCity} Key:{airConnection.Key} " +
                $"Model: {airConnection.AirplaneModel} Price: {airConnection.Price} Day: {airConnection.NumberOfDays} Time: {airConnection.dateTime.Hour}:{airConnection.dateTime.Minute}");
        }
        public void PrintClient(Client client)
        {
            Console.WriteLine($"Client ID: {client.Id} Name: {client.Name} Surname: {client.Surname} Born: {client.DateOfBorn.Day}.{client.DateOfBorn.Month}.{client.DateOfBorn.Year} " +
                $"Distance travelled {Math.Round(client.DistanceTravelled,3)}");
        }
        public void PrintTicketHistory(Ticket history)
        {
            Console.WriteLine($"{history.Id} {history.Key} {history.NumberOfDays} {history.dateTime.Hour}:{history.dateTime.Minute} " +
                    $"{history.AirportCity} {history.Price} {history.OwnerId} {Math.Round(history.Distance, 3)}");
        }       
    }
}
