﻿using FluentAssertions;
using FlightManagement.AirportClient.BusinessLayer.Services;
using Moq;
using NUnit.Framework;
using FlightManagement.AirportClient.DataLayer.Models;
using System;

namespace FlightManagement.AirportClient.BusinessLayer.Tests
{
    public class AgeCalculatorTests
    {
        [Test]
        public void ChildHasBornNow_DiscountIs1()
        {
            var child = DateTime.Now;
            var test = new AgeCalculatorService();
            var execute = test.CalculateAgeAndGetDiscount(child);

            execute.Should().Be(1);
        }

        [Test]
        public void AgeIs25_NoDiscount()
        {
            var child = DateTime.Now.AddYears(-25);
            var test = new AgeCalculatorService();
            var execute = test.CalculateAgeAndGetDiscount(child);

            execute.Should().Be(0);
        }

        [Test]
        public void AgeIs6_DiscountIsHalf()
        {
            var child = DateTime.Now.AddYears(-5);
            var test = new AgeCalculatorService();
            var execute = test.CalculateAgeAndGetDiscount(child);

            execute.Should().Be(0.5);
        }

        [Test]
        public void AgeIsUnderZero_DiscountIs2()
        {
            var child = DateTime.Now.AddYears(5);
            var test = new AgeCalculatorService();
            var execute = test.CalculateAgeAndGetDiscount(child);

            execute.Should().Be(2);
        }
    }
}
