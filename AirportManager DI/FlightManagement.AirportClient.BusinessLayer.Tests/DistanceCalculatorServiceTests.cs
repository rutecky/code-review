﻿using System;
using FluentAssertions;
using FlightManagement.AirportClient.BusinessLayer.Services;
using Moq;
using NUnit.Framework;
using FlightManagement.AirportClient.DataLayer.Models;

namespace FlightManagement.AirportClient.BusinessLayer.Tests
{
    public class DistanceCalculatorServiceTests
    {
        [Test]
        public void GeographicalCoordinatesAreTheSame_Return_0()
        {
            var airport = new AirConnection { AirportLatitude = 22.22, AirportLongitude = 55.55 };
            var test = new DistanceCalculatorService();
            var execute = test.GetDistanceToAirport(airport.AirportLatitude,airport.AirportLongitude);

            execute.Should().Be(0);      
        }

        [Test]
        public void DifferentAtLatitudeIsOnePoint_Return_Around69Miles()
        {
            var airport = new AirConnection { AirportLatitude = 21.22, AirportLongitude = 55.55 };
            var test = new DistanceCalculatorService();
            var execute = test.GetDistanceToAirport(airport.AirportLatitude, airport.AirportLongitude);

            execute.Should().BeApproximately(69.8,69.3);
        }
        [Test]
        public void DistanceTo0_0_Around_4047()
        {
            var airport = new AirConnection { AirportLatitude = 0, AirportLongitude = 0 };
            var test = new DistanceCalculatorService();
            var execute = test.GetDistanceToAirport(airport.AirportLatitude, airport.AirportLongitude);

            execute.Should().BeInRange(4035, 4037);
        }
        [Test]
        public void GeographicalCoordinatesAreOpposed_Return_8073Miles()
        {
            var airport = new AirConnection { AirportLatitude = -22.22, AirportLongitude = -55.55 };
            var test = new DistanceCalculatorService();
            var execute = test.GetDistanceToAirport(airport.AirportLatitude, airport.AirportLongitude);

            execute.Should().BeInRange(8072, 8074);

            //source : https://gps-coordinates.org/distance-between-coordinates.php
        }
    }
}
