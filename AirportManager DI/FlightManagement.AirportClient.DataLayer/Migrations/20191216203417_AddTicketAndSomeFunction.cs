﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FlightManagement.AirportClient.DataLayer.Migrations
{
    public partial class AddTicketAndSomeFunction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "DistanceTravelled",
                table: "Clients",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "AirportLatitude",
                table: "AirConnections",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "AirportLongitude",
                table: "AirConnections",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "AirConnections",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "ConnectionId",
                table: "AirConnections",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "Distance",
                table: "AirConnections",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "OwnerId",
                table: "AirConnections",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "isCanceled",
                table: "AirConnections",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DistanceTravelled",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "AirportLatitude",
                table: "AirConnections");

            migrationBuilder.DropColumn(
                name: "AirportLongitude",
                table: "AirConnections");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "AirConnections");

            migrationBuilder.DropColumn(
                name: "ConnectionId",
                table: "AirConnections");

            migrationBuilder.DropColumn(
                name: "Distance",
                table: "AirConnections");

            migrationBuilder.DropColumn(
                name: "OwnerId",
                table: "AirConnections");

            migrationBuilder.DropColumn(
                name: "isCanceled",
                table: "AirConnections");
        }
    }
}
