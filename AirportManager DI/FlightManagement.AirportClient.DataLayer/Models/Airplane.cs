﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlightManagement.AirportClient.DataLayer.Models
{
    public class Airplane : Entity
    {
        public string Producer { get; set; }
        public string Model { get; set; }
        public int NumberOfSeats { get; set; }
    }
}
