﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlightManagement.AirportClient.DataLayer.Models
{
    public class Client : Entity
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime DateOfBorn { get; set; }
        public double DistanceTravelled { get; set; }
    }
}
