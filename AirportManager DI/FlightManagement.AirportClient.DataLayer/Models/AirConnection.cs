﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlightManagement.AirportClient.DataLayer.Models
{    
    public class AirConnection : Entity
    {
        public string AirplaneModel { get; set; }
        public string AirportCity { get; set; }
        public double AirportLatitude { get; set; }
        public double AirportLongitude { get; set; }
        public int NumberOfDays{ get; set; }
        public string Key { get; set; }
        public double Price { get; set; }
        public DateTime dateTime { get; set; }
    }
}
