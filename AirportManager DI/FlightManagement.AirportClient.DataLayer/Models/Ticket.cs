﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlightManagement.AirportClient.DataLayer.Models
{
    public class Ticket : Entity
    {
        public int OwnerId { get; set; }
        public int ConnectionId { get; set; }
        public bool isCanceled { get; set; }       
        public double Distance { get; set; }
        public double Price { get; set; }
        public string Key { get; set; }
        public string AirportCity { get; set; }
        public DateTime dateTime { get; set; }
        public int NumberOfDays { get; set; }

    }
}
