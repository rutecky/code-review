﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlightManagement.AirportClient.DataLayer.Models
{
    public class Airports : Entity
    {
        public string AirportName { get; set; }
        public string AirportCountryName { get; set; }

        public string AirportCityName { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
