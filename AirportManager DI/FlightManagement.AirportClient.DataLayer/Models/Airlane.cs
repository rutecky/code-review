﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlightManagement.AirportClient.DataLayer.Models
{
    public class Airlane : Entity
    {
        public string Name { get; set; }
        public string Key { get; set; }
    }
}
