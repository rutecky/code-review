﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Text;

namespace FlightManagement.AirportClient.DataLayer.Bootstrapp
{
    public class DataLayerNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<IFlightManagementAirportClientDbContext>().ToMethod(x => new FlightManagementAirportClientDbContext());
        }
    }
}
