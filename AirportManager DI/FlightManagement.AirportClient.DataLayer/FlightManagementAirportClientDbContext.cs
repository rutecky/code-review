﻿using System;
using FlightManagement.AirportClient.DataLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace FlightManagement.AirportClient.DataLayer
{
    public interface IFlightManagementAirportClientDbContext : IDisposable
    {
        DbSet<Airports> Airports { get; set; }
        DbSet<Client> Clients { get; set; }
        DbSet<Airplane> Airplanes { get; set; }
        DbSet<AirConnection> AirConnections { get; set; }
        DbSet<Airlane> Airlanes { get; set; }
        DbSet<Ticket> Tickets { get; set; }
        DatabaseFacade Database { get; }
        int SaveChanges();
    }
    public class FlightManagementAirportClientDbContext : DbContext, IFlightManagementAirportClientDbContext
    {
        private const string _connectionString = "Data Source=.;Initial Catalog=HW_Airport_Rutecki_official12234;Integrated Security=True;";
        public DbSet<Airports> Airports { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Airplane> Airplanes { get; set; }
        public DbSet<AirConnection> AirConnections { get; set; }
        public DbSet<Airlane> Airlanes { get; set; }
        public DbSet<Ticket> Tickets { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}

